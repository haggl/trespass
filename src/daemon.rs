use std::fs::remove_file;
use std::io::{Read, Write};
use std::net::Shutdown;
use std::os::unix::net::{UnixListener, UnixStream};

use anyhow::Result;
use listenfd::ListenFd;

use crate::cli;
use crate::config;
use crate::recipient::SshKeypair;

fn handle_client(mut stream: UnixStream, cached_keys: &mut Vec<SshKeypair>) -> Result<()> {
    let mut incoming = String::from("");
    stream.read_to_string(&mut incoming)?;
    println!("Executing {}", incoming);
    for line in incoming.lines() {
        for output in cli::interpret_input(line, cached_keys) {
            stream.write_all(format!("{}\n", output).as_bytes())?;
        }
    }
    Ok(())
}

pub fn listen() -> Result<()> {
    let listener = match ListenFd::from_env().take_unix_listener(0).unwrap() {
        Some(l) => l,
        None => {
            let socket_path = config::get().socket_path();
            let _ = remove_file(&socket_path);
            UnixListener::bind(socket_path).unwrap()
        },
    };
    let mut cached_keys = vec![];
    loop {
        match listener.accept() {
            Ok((stream, _)) => {
                if let Err(err) = handle_client(stream, &mut cached_keys) {
                    println!("Error: {}", err);
                }
            },
            Err(err) => {
                println!("Error: {}", err);
                break;
            },
        }
    }
    Ok(())
}

pub fn rpc(cmd: String) -> Result<String> {
    let mut stream = UnixStream::connect(config::get().socket_path())?;
    stream.write_all(cmd.as_bytes())?;
    stream.shutdown(Shutdown::Write)?;
    let mut response = String::new();
    stream.read_to_string(&mut response)?;
    Ok(response.trim().to_owned())
}
