use std::fmt::{Display, Error, Formatter};
use std::fs::{create_dir_all, read_to_string, write};
use std::path::{Path, PathBuf};

use anyhow::Result;

use crate::config::RECIPIENTS_FILENAME;
use crate::repository;
use crate::secret;

/// SSH keypair
#[derive(Clone, Debug)]
pub struct SshKeypair {
    /// Key name / comment
    pub name: Option<String>,

    /// Key algorithm
    pub algorithm: String,

    /// Public key
    pub pubkey: String,

    /// Passphrase for encrypted private key
    pub passphrase: Option<String>,
}

impl SshKeypair {
    pub fn from(key: &str) -> Self {
        let mut tokens = key.split(' ');
        let mut result = SshKeypair {
            name: None,
            algorithm: String::from(tokens.next().unwrap()),
            pubkey: String::from(tokens.next().unwrap()),
            passphrase: None,
        };
        if let Some(name) = tokens.next() {
            result.name = Some(String::from(name));
        }
        result
    }
}

impl Display for SshKeypair {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        let mut line = [self.algorithm.clone(), self.pubkey.clone()].join(" ");
        if let Some(name) = &self.name {
            line = [line, name.to_string()].join(" ");
        }
        write!(f, "{}", line)
    }
}

fn read_recipients(path: &Path) -> Vec<SshKeypair> {
    match read_to_string(path) {
        Ok(c) => c.lines().map(|k| SshKeypair::from(&String::from(k))).collect(),
        Err(_) => Vec::new(),
    }
}

/// Lists recipients of a collection
fn list_recipients(path: &PathBuf, verbose: bool) -> Result<Vec<SshKeypair>> {
    let mut collection_path = repository::collection_path(path)?;
    loop {
        let recipients_file = collection_path.join(RECIPIENTS_FILENAME);
        let recipients = read_recipients(&recipients_file);
        if recipients.is_empty() {
            if collection_path == repository::path(&PathBuf::from(".")) {
                return Ok(recipients);
            }
            collection_path = PathBuf::from(collection_path.parent().unwrap());
            if verbose {
                println!(
                    "Access to {} inherited from {}",
                    path.to_str().unwrap(),
                    repository::relative_path(&collection_path).to_str().unwrap()
                );
            }
            continue;
        }
        return Ok(recipients);
    }
}

pub fn add(path: &PathBuf, pubkey: &str, cached_keys: &mut Vec<SshKeypair>) -> Result<Vec<String>> {
    let mut results = vec![];
    let new_key = SshKeypair::from(pubkey);
    results.push(format!(
        "Granting {} access to {}",
        new_key.name.clone().unwrap_or(new_key.pubkey.clone()),
        path.to_str().unwrap()
    ));

    let mut recipients = list(path)?;
    for key in &recipients {
        if key.algorithm != new_key.algorithm {
            continue;
        }
        if key.pubkey != new_key.pubkey {
            continue;
        }
        results.push("Access already granted".to_string());
        return Ok(results);
    }
    recipients.push(new_key);

    let collection_path = repository::collection_path(path)?;
    create_dir_all(collection_path.as_path())?;
    let recipients_file = collection_path.join(RECIPIENTS_FILENAME);
    let recipients_lines: Vec<String> = recipients.iter().map(SshKeypair::to_string).collect();
    write(&recipients_file, recipients_lines.join("\n"))?;
    let message = format!("Update recipients for {}", path.to_str().unwrap());
    repository::update_file(&path.join(RECIPIENTS_FILENAME), message)?;
    secret::reencrypt(path, recipients, cached_keys)?;
    Ok(results)
}

/// Lists recipients of a collection
pub fn list(path: &PathBuf) -> Result<Vec<SshKeypair>> {
    list_recipients(path, false)
}

pub fn print(path: &PathBuf) -> Result<Vec<String>> {
    return Ok(list_recipients(path, true)?.iter().map(|key| format!("{}", key)).collect());
}

pub fn remove(
    path: &PathBuf,
    pubkey: &str,
    cached_keys: &mut Vec<SshKeypair>,
) -> Result<Vec<String>> {
    let mut results = vec![];
    let obsolete_key = SshKeypair::from(pubkey);
    results.push(format!(
        "Revoking {} access to {}",
        obsolete_key.name.clone().unwrap_or(obsolete_key.pubkey.clone()),
        path.to_str().unwrap()
    ));
    let collection_path = repository::collection_path(path)?;
    let mut recipients = list(&collection_path)?;
    let len_before = recipients.len();
    recipients.retain(|k| {
        if k.algorithm == obsolete_key.algorithm && k.pubkey == obsolete_key.pubkey {
            return false;
        }
        true
    });
    if recipients.len() == len_before {
        results.push("Nothing to do".to_string());
        return Ok(results);
    }
    create_dir_all(collection_path.as_path())?;
    let recipients_lines: Vec<String> = recipients.iter().map(SshKeypair::to_string).collect();
    write(collection_path.join(RECIPIENTS_FILENAME), recipients_lines.join("\n"))?;
    let message = format!("Update recipients for {}", path.to_str().unwrap());
    repository::update_file(&path.join(RECIPIENTS_FILENAME), message)?;
    secret::reencrypt(path, recipients, cached_keys)?;
    Ok(results)
}
