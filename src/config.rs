use std::path::PathBuf;
use std::sync::OnceLock;

use config::{Config, Environment, File};
use dirs::{config_dir, data_dir, home_dir, runtime_dir};
use serde::Deserialize;

//
// Helper functions
//

fn expand_relative(path: &String) -> PathBuf {
    let path = PathBuf::from(path);
    if path.is_absolute() {
        return path;
    }
    home_dir().unwrap().join(path)
}

fn read_config() -> Configuration {
    let mut config = Config::builder();
    let config_path = config_dir().unwrap().join("trespass/config.toml");
    if config_path.metadata().is_ok() {
        config = config.add_source(File::from(config_path));
    }
    config
        .add_source(Environment::with_prefix("trespass"))
        .build()
        .unwrap()
        .try_deserialize()
        .unwrap()
}

//
// Default values for dynamic configuration
//

fn clip_timeout_default() -> u64 {
    30
}

fn hist_path_default() -> String {
    data_dir().unwrap().join("trespass/history").to_str().unwrap().to_owned()
}

fn notification_timeout_default() -> u32 {
    4
}

fn pinentry_program_default() -> String {
    "pinentry".to_owned()
}

fn repo_path_default() -> String {
    data_dir().unwrap().join("trespass/repository").to_str().unwrap().to_owned()
}

fn secret_length_default() -> u32 {
    42
}

fn socket_path_default() -> String {
    runtime_dir().unwrap().join("trespass.sock").to_str().unwrap().to_owned()
}

fn ssh_path_default() -> String {
    home_dir().unwrap().join(".ssh").to_str().unwrap().to_owned()
}

//
// Dynamic configuration
//

#[derive(Debug, Deserialize)]
#[allow(unused)]
pub struct Configuration {
    /// Clipboard timeout in seconds
    #[serde(default = "clip_timeout_default")]
    clip_timeout: u64,

    /// Interactive history path
    #[serde(default = "hist_path_default")]
    hist_path: String,

    /// Notification timeout in seconds
    #[serde(default = "notification_timeout_default")]
    notification_timeout: u32,

    /// Pinentry program
    #[serde(default = "pinentry_program_default")]
    pinentry_program: String,

    /// Secret repository path
    #[serde(default = "repo_path_default")]
    repo_path: String,

    /// Default length of generated secrets
    #[serde(default = "secret_length_default")]
    secret_length: u32,

    /// Unix domain socket path
    #[serde(default = "socket_path_default")]
    socket_path: String,

    /// SSH pubkey directory path
    #[serde(default = "ssh_path_default")]
    ssh_path: String,
}

impl Configuration {
    pub fn clip_timeout(&self) -> u64 {
        self.clip_timeout
    }

    pub fn hist_path(&self) -> PathBuf {
        expand_relative(&self.hist_path)
    }

    pub fn notification_timeout(&self) -> u32 {
        self.notification_timeout * 1000
    }

    pub fn pinentry_program(&self) -> &str {
        &self.pinentry_program
    }

    pub fn repo_path(&self) -> PathBuf {
        expand_relative(&self.repo_path)
    }

    pub fn secret_length(&self) -> u32 {
        self.secret_length
    }

    pub fn socket_path(&self) -> PathBuf {
        expand_relative(&self.socket_path)
    }

    pub fn ssh_path(&self) -> PathBuf {
        expand_relative(&self.ssh_path)
    }
}

/// Singleton getter
pub fn get() -> &'static Configuration {
    static SINGLETON: OnceLock<Configuration> = OnceLock::new();
    SINGLETON.get_or_init(read_config)
}

//
// Static configuration
//

/// File name of text files storing allowed recipients of a subtree
pub const RECIPIENTS_FILENAME: &str = ".recipients";

//
// Tests
//

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn expand_relative() {
        assert_eq!(super::expand_relative(&"test".to_owned()), home_dir().unwrap().join("test"));
        assert_eq!(super::expand_relative(&"/tmp".to_owned()), PathBuf::from("/tmp"));
    }

    #[test]
    #[ignore = "must find a way to mock / patch actual config file parsing"]
    fn defaults() {
        assert_eq!(super::get().clip_timeout(), clip_timeout_default());
        assert_eq!(super::get().hist_path(), PathBuf::from(hist_path_default()));
        assert_eq!(super::get().notification_timeout(), notification_timeout_default() * 1000);
        assert_eq!(super::get().pinentry_program(), pinentry_program_default());
        assert_eq!(super::get().repo_path(), PathBuf::from(repo_path_default()));
        assert_eq!(super::get().socket_path(), PathBuf::from(socket_path_default()));
        assert_eq!(super::get().ssh_path(), PathBuf::from(ssh_path_default()));
    }
}
