use std::path::{Path, PathBuf};
use std::process::Command;

use anyhow::{anyhow, Result};
use gix::interrupt::IS_INTERRUPTED;
use gix::progress::Discard;

use crate::config;

//
// Internal functions
//

fn execute_command(args: &Vec<&str>, repo: Option<&PathBuf>) -> Result<Vec<String>> {
    let repo = gix::open(repo.unwrap_or(&root_path()))?;
    let ctx = repo.command_context()?;
    let mut command = Command::from(gix_command::prepare("git").with_context(ctx));
    command.args(args);
    command.current_dir(repo.work_dir().unwrap());
    let output = String::from_utf8(command.output()?.stdout)?;
    return Ok(output.lines().map(String::from).collect());
}

fn resolve_submodule_path(path: &Path) -> Result<(String, PathBuf)> {
    let mut file_path = path.to_path_buf();
    let mut repo_path = root_path();
    if let Ok(Some(submodules)) = gix::open(root_path())?.submodules() {
        for submodule in submodules {
            let prefix = submodule.path()?.as_ref().to_string();
            if path.starts_with(&prefix) {
                repo_path = submodule.work_dir()?;
                file_path = path.strip_prefix(prefix)?.to_path_buf();
                break;
            }
        }
    }
    Ok((file_path.to_str().unwrap().to_string(), repo_path))
}

fn root_path() -> PathBuf {
    config::get().repo_path()
}

fn update_submodule(path: PathBuf) -> Result<()> {
    if path != root_path() {
        let submodule_path = path.strip_prefix(root_path())?.to_str().unwrap();
        let message = format!("Update submodule {}", &submodule_path);
        execute_command(&vec!["stage", &submodule_path], None)?;
        execute_command(&vec!["commit", "-m", &message], None)?;
    }
    Ok(())
}

//
// Public API
//

pub fn add(path: &Path, url: &String) -> Result<Vec<String>> {
    let mut output = vec![];
    // This appears to be unsupported - maybe check if it works later or implement it there:
    // output.append(&mut execute_command(vec!["submodule", "add", url, path.to_str().unwrap()])?);
    // Instead we do it manually
    let mut cmd = Command::new("git");
    cmd.args(["submodule", "add", url, path.to_str().unwrap()]);
    cmd.current_dir(config::get().repo_path());
    output.push(String::from_utf8(cmd.output().unwrap().stdout).unwrap());
    let message = format!("Add submodule {} at {}", url, path.to_str().unwrap());
    execute_command(&vec!["commit", "-m", message.as_str()], None)?;
    output.push(message);
    Ok(output)
}

pub fn clone(url: &String) -> Result<Vec<String>> {
    let url = gix::Url::from_bytes(url.as_bytes().into())?;
    let mut repo = gix::prepare_clone(url, root_path())?;
    let (mut repo, _) = repo.fetch_then_checkout(Discard, &IS_INTERRUPTED)?;
    let _ = repo.main_worktree(Discard, &IS_INTERRUPTED)?;
    let mut messages = vec!["Clone successful".to_owned()];
    let command = vec!["submodule", "update", "--init", "--recursive"];
    messages.append(&mut execute_command(&command, None)?);
    Ok(messages)
}

pub fn collection_path(path: &PathBuf) -> Result<PathBuf> {
    let collection_path = self::path(path);
    if let Ok(metadata) = collection_path.metadata() {
        if !metadata.is_dir() {
            return Err(anyhow!("Given path is not a collection!"));
        }
    }
    Ok(collection_path)
}

pub fn init() {
    gix::init(root_path()).expect("Error initializing repository");
}

pub fn list(path: &Option<PathBuf>) -> Result<Vec<String>> {
    let mut command = vec!["ls-files", "--recurse-submodules"];
    if let Some(p) = path {
        command.push(p.to_str().unwrap());
    }
    let mut secrets = execute_command(&command, None)?;
    secrets.retain(|s| !s.ends_with(".gitmodules"));
    Ok(secrets)
}

pub fn path(path: &PathBuf) -> PathBuf {
    root_path().join(path)
}

pub fn relative_path(path: &Path) -> PathBuf {
    return path.strip_prefix(root_path()).unwrap().to_path_buf();
}

pub fn remove(path: &PathBuf) -> Result<Vec<String>> {
    let target_type = if self::path(path).is_dir() { "collection" } else { "secret" };
    let (target_path, repo_path) = resolve_submodule_path(path)?;
    let message = format!("Remove {} {}", target_type, target_path);
    execute_command(&vec!["rm", "-r", &target_path], Some(&repo_path))?;
    execute_command(&vec!["commit", "-m", message.as_str()], Some(&repo_path))?;
    update_submodule(repo_path)?;
    Ok(vec![message])
}

pub fn sync() -> Result<Vec<String>> {
    let mut output = vec![];
    output.append(&mut execute_command(&vec!["pull", "--recurse-submodules=on-demand"], None)?);
    output.append(&mut execute_command(&vec!["push", "--recurse-submodules=on-demand"], None)?);
    Ok(output)
}

pub fn update_file(path: &Path, message: String) -> Result<Vec<String>> {
    let (file_path, repo_path) = resolve_submodule_path(path)?;
    execute_command(&vec!["stage", &file_path], Some(&repo_path))?;
    execute_command(&vec!["commit", "-m", message.as_str()], Some(&repo_path))?;
    update_submodule(repo_path)?;
    Ok(vec![message])
}
