use std::fs::create_dir_all;
use std::path::PathBuf;

use clap::{Command, CommandFactory};
use rustyline::error::ReadlineError;
use shlex::split;

use rustyline::completion::Completer;
use rustyline::hint::HistoryHinter;
use rustyline::history::FileHistory;
use rustyline::{CompletionType, Config, Editor, Result};
use rustyline::{Context, Helper, Highlighter, Hinter, Validator};

use crate::cli;
use crate::config;
use crate::daemon;
use crate::recipient::SshKeypair;
use crate::repository;

#[derive(Helper, Highlighter, Hinter, Validator)]
pub struct CommandHelper(#[rustyline(Hinter)] HistoryHinter);

impl Completer for CommandHelper {
    type Candidate = String;

    fn complete(&self, line: &str, pos: usize, _ctx: &Context<'_>) -> Result<(usize, Vec<String>)> {
        let mut matches: Vec<String> = vec![];
        let mut match_subcommands = |token: &str, len: usize, cmd: &Command| -> Option<Command> {
            for cmd in cmd.get_subcommands() {
                let cmd_name = cmd.get_name();
                if cmd_name == token {
                    return Some(cmd.clone());
                }
                if len < cmd_name.len() && cmd_name[..len] == *token {
                    matches.push(format!("{} ", cmd_name).to_string());
                }
            }
            None
        };

        let tokens = split(line).unwrap();
        let mut tokens_iter = tokens.iter();

        let cmd_tok = match tokens_iter.next() {
            Some(t) => t,
            None => "",
        };
        let command = match match_subcommands(cmd_tok, pos, &cli::Command::command()) {
            Some(c) => c,
            None => return Ok((0, matches)),
        };

        let sub_tok = match tokens_iter.next() {
            Some(t) => t,
            None => "",
        };
        let len = (pos - command.get_name().len()).saturating_sub(1);
        let subcommand = match match_subcommands(sub_tok, len, &command) {
            Some(c) => c,
            None => return Ok((command.get_name().len() + 1, matches)),
        };

        if subcommand.get_positionals().all(|arg| arg.get_id() != "path") {
            return Ok((pos, vec![]));
        }

        let path_tok = match tokens_iter.last() {
            Some(t) => t,
            None => "",
        };
        let pos = command.get_name().len() + subcommand.get_name().len() + 2;
        if let Ok(secrets) = repository::list(&None) {
            for secret in secrets {
                if secret.ends_with(config::RECIPIENTS_FILENAME) {
                    continue;
                }
                let mut path = PathBuf::from(secret.clone());
                while let Some(parent) = path.parent() {
                    if parent != PathBuf::from("") {
                        let dir = parent.join("").to_str().unwrap().to_string();
                        if !matches.contains(&dir) && dir.starts_with(path_tok) && dir != path_tok {
                            matches.push(dir);
                        }
                    }
                    path = parent.to_path_buf();
                }
                if secret.starts_with(path_tok) {
                    let path = PathBuf::from(secret.clone());
                    let parent = path.parent().unwrap().join("");
                    if !matches.contains(&parent.to_str().unwrap().to_string()) {
                        matches.push(secret);
                    }
                }
            }
        }

        Ok((pos, matches))
    }
}

pub type CommandEditor = Editor<CommandHelper, FileHistory>;

pub struct Interpreter {
    editor: CommandEditor,
    history: PathBuf,
    cached_keys: Vec<SshKeypair>,
}

impl Interpreter {
    pub fn new() -> Self {
        let histfile = config::get().hist_path();
        create_dir_all(histfile.parent().unwrap()).unwrap();

        let mut rl_config = Config::builder();
        rl_config = rl_config.auto_add_history(true);
        rl_config = rl_config.completion_type(CompletionType::List);

        let mut rl = CommandEditor::with_config(rl_config.build()).unwrap();
        rl.set_helper(Some(CommandHelper(HistoryHinter::new())));
        if rl.load_history(&histfile).is_err() {
            println!("No previous history");
        }
        Self { editor: rl, history: histfile, cached_keys: vec![] }
    }

    pub fn read_line(&mut self) -> Result<String> {
        self.editor.readline("> ")
    }

    pub fn append_history(&mut self) {
        self.editor.append_history(&self.history).unwrap();
    }

    pub fn run(&mut self, standalone: bool) -> Result<()> {
        let mut interpreter = Interpreter::new();
        loop {
            let cmd = match interpreter.read_line() {
                Ok(line) => line,
                Err(ReadlineError::Eof) => break,
                Err(_) => continue,
            };

            if standalone || cmd.starts_with("secret edit") {
                let result = cli::interpret_input(&cmd, &mut self.cached_keys);
                for line in result {
                    println!("{}", line);
                }
            } else {
                match daemon::rpc(cmd) {
                    Ok(result) => println!("{}", result),
                    Err(err) => println!("{}", err),
                }
            }

            interpreter.append_history();
        }
        Ok(())
    }
}
