use std::path::PathBuf;

use age::DecryptError;
use anyhow::{anyhow, Result};
use clap::{Parser, Subcommand, ValueEnum};
use clap_complete::Shell;
use shlex::split;

use crate::recipient;
use crate::recipient::SshKeypair;
use crate::repository;
use crate::secret;

/// Team-Ready Enhanced Substitute for PASS
///
/// A pass-like secret manager for teams driven by age encryption and SSH keys.
/// Because gopass is painful and passage lacks support for multiple users.
#[derive(Parser)]
#[command(version)]
pub struct Cli {
    /// Do not use daemon
    #[arg(short, long)]
    pub standalone: bool,

    /// Verbose output
    #[arg(short, long)]
    pub verbose: bool,

    #[command(subcommand)]
    pub command: Option<Command>,
}

#[derive(Parser)]
pub enum Command {
    /// Print shell completion rules
    Completion { shell: Shell },

    /// Open unix socket and wait for commands
    Daemon {},

    /// Show or modify recipients for collection
    Recipient {
        #[command(subcommand)]
        action: RecipientAction,
    },

    /// Show or modify recipients for collection
    Repository {
        #[command(subcommand)]
        action: RepositoryAction,
    },

    /// Show or modify secret
    Secret {
        #[command(subcommand)]
        action: SecretAction,
    },
}

#[derive(Subcommand)]
pub enum RecipientAction {
    /// Add recipient
    Add {
        /// SSH public key
        key: String,

        /// Collection or secret
        path: Option<PathBuf>,
    },

    /// List recipients
    List {
        /// Collection or secret
        path: Option<PathBuf>,
    },

    /// Remove given SSH key from recipients of a collection or secret
    Remove {
        /// SSH public key
        key: String,

        /// Collection or secret
        path: Option<PathBuf>,
    },
}

#[derive(Subcommand)]
pub enum RepositoryAction {
    /// Add a submodule
    Add {
        /// Path to submodule
        path: PathBuf,

        /// Upstream repository URL
        url: String,
    },

    /// Clone upstream secrets repository
    Clone {
        /// Upstream repository URL
        url: String,
    },

    /// Initialize repository
    Init {
        /// SSH public key
        key: String,
    },

    /// Synchronize with upstream
    Sync {},

    /// Remove a submodule
    Remove {
        /// Path to submodule
        path: PathBuf,
    },
}

#[derive(ValueEnum, Clone)]
pub enum Metadata {
    Comment,
    Location,
    Username,
}

#[derive(Subcommand)]
pub enum SecretAction {
    /// Add a secret
    Add {
        /// Location name to store in metadata
        #[arg(short, long)]
        location: Option<String>,

        /// User name to store in metadata
        #[arg(short, long)]
        user: Option<String>,

        /// Path to secret
        path: PathBuf,

        /// Secret content
        secret: Option<String>,
    },

    /// Write secret to clipboard
    Clip {
        /// Clip both first line and selected metadata field
        #[arg(short, long)]
        both: bool,

        /// Path to secret
        path: PathBuf,

        /// Metadata field to clip
        #[clap(value_parser = ["comment", "location", "username"])]
        part: Option<String>,
    },

    /// Edit a secret (implies --standalone)
    Edit {
        /// Path to secret
        path: PathBuf,
    },

    /// Find a secret
    Find {
        /// Ignore case during search
        #[arg(short, long)]
        ignore_case: bool,

        /// Pattern to search for
        pattern: String,
    },

    /// Generate a new secret
    Generate {
        /// Copy generated secret to clipboard
        #[arg(short, long)]
        clip: bool,

        /// Location name to store in metadata
        #[arg(short, long)]
        location: Option<String>,

        /// Print generated secret
        #[arg(short, long)]
        show: bool,

        /// Include at least one symbol
        #[arg(short = 'S', long)]
        symbols: bool,

        /// User name to store in metadata
        #[arg(short, long)]
        user: Option<String>,

        /// Path to secret
        path: PathBuf,

        /// Length of the generated secret
        length: Option<u32>,
    },

    /// List secrets in collection
    List {
        /// Path to collection
        path: Option<PathBuf>,
    },

    /// Remove a secret
    Remove {
        /// Path to secret
        path: PathBuf,
    },

    /// Show secret
    Show {
        /// Copy to clipboard instead of printing
        #[arg(short, long)]
        clip: bool,

        /// Path to secret
        path: PathBuf,
    },
}

pub fn recipient(
    action: &RecipientAction,
    cached_keys: &mut Vec<SshKeypair>,
) -> Result<Vec<String>> {
    fn ensure(path: &Option<PathBuf>) -> PathBuf {
        match path {
            Some(p) => p.to_path_buf(),
            None => PathBuf::from("."),
        }
    }
    match action {
        RecipientAction::Add { key, path } => recipient::add(&ensure(path), key, cached_keys),
        RecipientAction::List { path } => recipient::print(&ensure(path)),
        RecipientAction::Remove { key, path } => recipient::remove(&ensure(path), key, cached_keys),
    }
}

pub fn repository(action: &RepositoryAction) -> Result<Vec<String>> {
    match action {
        RepositoryAction::Add { path, url } => repository::add(path, url),
        RepositoryAction::Clone { url } => repository::clone(url),
        RepositoryAction::Init { key } => {
            repository::init();
            recipient::add(&PathBuf::from("."), key, &mut vec![])
        },
        RepositoryAction::Sync {} => repository::sync(),
        RepositoryAction::Remove { path } => repository::remove(path),
    }
}

pub fn secret(action: &SecretAction, cached_keys: &mut Vec<SshKeypair>) -> Result<Vec<String>> {
    match action {
        SecretAction::Add { path, secret, user, location } => {
            secret::add(path, secret, user, location)
        },
        SecretAction::Clip { path, both, part } => secret::clip(path, both, part, cached_keys),
        SecretAction::Edit { path } => secret::edit(path, cached_keys),
        SecretAction::Find { pattern, ignore_case } => secret::find(pattern, ignore_case),
        SecretAction::Generate { path, length, symbols, clip, show, user, location } => {
            secret::generate(path, length, symbols, clip, show, user, location)
        },
        SecretAction::List { path } => secret::list(path),
        SecretAction::Remove { path } => secret::remove(path),
        SecretAction::Show { path, clip } => secret::show(path, clip, cached_keys),
    }
}

pub fn interpret(command: &Command, cached_keys: &mut Vec<recipient::SshKeypair>) -> Vec<String> {
    let result = match command {
        Command::Recipient { action } => recipient(action, cached_keys),
        Command::Repository { action } => repository(action),
        Command::Secret { action } => secret(action, cached_keys),
        _ => Err(anyhow!("Not implemented")),
    };
    match result {
        Ok(res) => res,
        Err(err) => {
            let message = format!("{}", err);
            if let Ok(DecryptError::KeyDecryptionFailed) = err.downcast::<DecryptError>() {
                cached_keys.pop();
            }
            vec![message]
        },
    }
}

pub fn interpret_input(input: &str, cached_keys: &mut Vec<recipient::SshKeypair>) -> Vec<String> {
    let mut tokens: Vec<String> = split(input).unwrap();
    tokens.insert(0, String::from("trespass"));

    match Command::try_parse_from(tokens) {
        Ok(command) => interpret(&command, cached_keys),
        Err(error) => vec![format!("{}", error)],
    }
}
