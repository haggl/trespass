mod cli;
mod config;
mod daemon;
mod interactive;
mod recipient;
mod repository;
mod secret;

use std::env::args;
use std::io::stdout;

use clap::{Command, CommandFactory, Parser};
use clap_complete::{generate, Generator};

fn print_completions<G: Generator>(gen: G, cmd: &mut Command) {
    generate(gen, cmd, cmd.get_name().to_string(), &mut stdout());
}

fn main() {
    let cli = cli::Cli::parse();
    match cli.command {
        None => {
            interactive::Interpreter::new().run(cli.standalone).unwrap();
        },
        Some(cli::Command::Completion { shell }) => {
            print_completions(shell, &mut cli::Cli::command());
        },
        Some(cli::Command::Daemon {}) => {
            daemon::listen().unwrap();
        },
        Some(command) => {
            let tokens = &args().collect::<Vec<String>>()[1..];
            if !cli.standalone && tokens[0..2].join(" ") != "secret edit" {
                if let Ok(result) = daemon::rpc(tokens.join(" ")) {
                    println!("{}", result);
                    return;
                }
            }
            for line in cli::interpret(&command, &mut vec![]) {
                println!("{}", line);
            }
        },
    }
}
