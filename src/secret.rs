use std::env::var;
use std::fs::{create_dir_all, read, read_to_string, write, File};
use std::io::{BufReader, Read, Seek, SeekFrom, Write};
use std::path::{Path, PathBuf};
use std::process::Command;
use std::str::FromStr;
use std::thread::spawn;
use std::time::{Duration, Instant};

use age::secrecy::{ExposeSecret, SecretString};
use age::ssh;
use age::{Callbacks, Decryptor, Encryptor, Identity, Recipient};
use anyhow::{anyhow, Result};
use arboard::{Clipboard, LinuxClipboardKind, SetExtLinux};
use glob::glob;
use notify_rust::{Notification, Timeout};
use passwords::PasswordGenerator;
use pinentry::PassphraseInput;
use serde::Deserialize;
use tempfile::NamedTempFile;

use crate::config;
use crate::config::RECIPIENTS_FILENAME;
use crate::recipient;
use crate::recipient::SshKeypair;
use crate::repository;

//
// Data structures
//

#[derive(Deserialize)]
struct SecretMetadata {
    comment: Option<String>,
    location: Option<String>,
    username: Option<String>,
}

//
// Internal functions
//

fn copy_to_clipboard(target: LinuxClipboardKind, secret: String) -> Result<()> {
    let cfg = config::get();
    let timeout = Duration::from_secs(cfg.clip_timeout());
    let future = Instant::now().checked_add(timeout).unwrap();
    spawn(move || -> Result<()> {
        Clipboard::new()?.set().clipboard(target).wait_until(future).text(secret)?;
        Notification::new()
            .summary("trespass")
            .body("Clipboard cleared")
            .timeout(Timeout::Milliseconds(cfg.notification_timeout()))
            .show()?;
        Ok(())
    });
    Ok(())
}

fn decrypt(path: &Path, identities: &[impl Identity]) -> Result<String> {
    let encrypted = read(repository::path(&path.to_path_buf()))?;
    let decryptor = match Decryptor::new(&encrypted[..])? {
        Decryptor::Recipients(d) => d,
        _ => unreachable!(),
    };
    let mut decrypted = vec![];
    let mut reader = decryptor.decrypt(identities.iter().map(|i| i as &dyn Identity))?;
    reader.read_to_end(&mut decrypted)?;
    Ok(String::from_utf8(decrypted)?)
}

fn encrypt(
    path: &PathBuf,
    secret: &String,
    recipient_keys: &Vec<SshKeypair>,
    msg: Option<String>,
) -> Result<()> {
    let mut recipients: Vec<Box<(dyn Recipient + Send)>> = vec![];
    for key in recipient_keys {
        let rcpt = ssh::Recipient::from_str(&key.to_string()).expect("Could not parse SSH pubkey");
        recipients.push(Box::new(rcpt));
    }

    let encryptor = Encryptor::with_recipients(recipients).unwrap();
    let mut encrypted = vec![];
    let mut writer = encryptor.wrap_output(&mut encrypted)?;
    writer.write_all(secret.as_bytes())?;
    writer.finish()?;
    write(repository::path(path), encrypted)?;
    if let Some(msg) = msg {
        repository::update_file(path, msg)?;
    }
    Ok(())
}

fn fill_in_recipients(path: &Path, cached_keys: &mut Vec<SshKeypair>) -> Result<()> {
    for rcpt in list_recipients(path)? {
        let found = cached_keys
            .iter()
            .find(|key| rcpt.algorithm == key.algorithm && rcpt.pubkey == key.pubkey);
        if found.is_none() {
            cached_keys.push(rcpt);
        }
    }
    Ok(())
}

fn get_ssh_identities(recipients: &mut [SshKeypair], limit: usize) -> Result<Vec<impl Identity>> {
    let mut identities = vec![];
    let pattern = config::get().ssh_path().join("id_ed25519*.pub");
    for candidate in glob(pattern.to_str().unwrap()).expect("Could not find SSH keys") {
        let pubkey_path = match candidate {
            Ok(p) => p,
            Err(e) => {
                println!("{:?}", e);
                continue;
            },
        };
        let mut key = SshKeypair::from(&read_to_string(&pubkey_path)?);
        let found =
            recipients.iter().position(|r| r.algorithm == key.algorithm && r.pubkey == key.pubkey);
        let idx = match found {
            Some(i) => {
                key = recipients[i].clone();
                i
            },
            None => continue,
        };

        let pkey_path = String::from(pubkey_path.to_str().unwrap().strip_suffix(".pub").unwrap());
        let buffer = BufReader::new(File::open(&pkey_path).unwrap());
        let identity = ssh::Identity::from_buffer(buffer, Some(pkey_path.clone()))?;
        if key.passphrase.is_none() {
            let mut pinentry = match PassphraseInput::with_binary(config::get().pinentry_program())
            {
                Some(r) => r,
                None => return Err(anyhow!("Could not find pinentry")),
            };
            let desc = format!("Please enter passphrase to unlock {:?}:", pkey_path).to_string();
            let passphrase = match pinentry
                .with_title("trespass")
                .with_prompt("Passphrase:")
                .with_description(&desc)
                .interact()
            {
                Ok(r) => r.expose_secret().to_owned(),
                Err(e) => return Err(anyhow!("{:?}", e)),
            };
            key.passphrase = Some(passphrase.clone());
            recipients[idx].passphrase = Some(passphrase);
        }
        identities.push(identity.with_callbacks(key));
        if limit > 0 && identities.len() == limit {
            break;
        }
    }
    Ok(identities)
}

fn list_recipients(path: &Path) -> Result<Vec<SshKeypair>> {
    let collection = path.parent().unwrap().to_path_buf();
    recipient::list(&collection)
}

fn load(path: &Path, cached_keys: &mut Vec<SshKeypair>) -> Result<String> {
    let num = if cached_keys.is_empty() { 1 } else { 0 };
    fill_in_recipients(path, cached_keys)?;
    let identities = get_ssh_identities(cached_keys, num)?;
    decrypt(path, &identities)
}

//
// Public API
//

impl Callbacks for SshKeypair {
    fn display_message(&self, message: &str) {
        println!("{}", message);
    }

    fn confirm(&self, _message: &str, _yes_string: &str, _no_string: Option<&str>) -> Option<bool> {
        unimplemented!();
    }

    fn request_public_string(&self, _description: &str) -> Option<String> {
        unimplemented!();
    }

    fn request_passphrase(&self, _description: &str) -> Option<SecretString> {
        Some(SecretString::from(self.passphrase.clone()?))
    }
}

pub fn add(
    path: &PathBuf,
    secret: &Option<String>,
    user: &Option<String>,
    location: &Option<String>,
) -> Result<Vec<String>> {
    let mut secret = match secret {
        Some(s) => {
            if s.is_empty() {
                return Err(anyhow!("Empty secret"));
            }
            s.clone()
        },
        None => {
            let mut pinentry = match PassphraseInput::with_binary(config::get().pinentry_program())
            {
                Some(r) => r,
                None => return Err(anyhow!("Could not find pinentry")),
            };
            let description = format!("Please enter secret to store at {:?}: ", path).to_string();
            match pinentry
                .with_title("trespass")
                .with_prompt("Secret:")
                .with_description(&description)
                .with_confirmation("Confirm:", "Secrets do not match")
                .interact()
            {
                Ok(r) => r.expose_secret().to_owned(),
                Err(e) => return Err(anyhow!("{:?}", e)),
            }
        },
    };

    if let Some(user) = user {
        secret += &format!("\nuser = '''{}'''", user.clone());
    }
    if let Some(location) = location {
        secret += &format!("\nlocation = '''{}'''", location.clone());
    }

    create_dir_all(repository::path(path).parent().unwrap())?;
    let message = format!("Add secret {}", path.to_str().unwrap());
    encrypt(path, &secret, &list_recipients(path)?, Some(message))?;
    Ok(vec![])
}

pub fn clip(
    path: &Path,
    both: &bool,
    part: &Option<String>,
    cached_keys: &mut Vec<SshKeypair>,
) -> Result<Vec<String>> {
    let mut feedback = vec![];
    let mut secret = load(path, cached_keys)?;
    if let Some((head, tail)) = secret.clone().split_once('\n') {
        secret = head.to_owned();
        if let Some(part) = part {
            let metadata: SecretMetadata = toml::from_str(tail)?;
            let field = match part.as_str() {
                "comment" => metadata.comment,
                "location" => metadata.location,
                "username" => metadata.username,
                _ => unreachable!(),
            };
            if let Some(field) = field {
                if *both {
                    copy_to_clipboard(LinuxClipboardKind::Primary, field).unwrap_or_else(|_| {
                        feedback.push("Copying to primary clipboard failed".to_string());
                    });
                } else {
                    secret = field;
                }
            } else {
                feedback.push(format!("metadata does not contain {}", part));
            }
        }
    }
    if feedback.is_empty() {
        copy_to_clipboard(LinuxClipboardKind::Clipboard, secret)?;
    }
    Ok(feedback)
}

pub fn edit(path: &PathBuf, cached_keys: &mut Vec<SshKeypair>) -> Result<Vec<String>> {
    let mut tmp_file = NamedTempFile::new()?;
    tmp_file.write_all(load(path, cached_keys)?.as_bytes())?;
    tmp_file.seek(SeekFrom::Start(0))?;
    Command::new(var("EDITOR")?).arg(tmp_file.path()).status()?;
    let mut new_secret = String::new();
    tmp_file.read_to_string(&mut new_secret)?;
    let message = format!("Update secret {}", path.to_str().unwrap());
    encrypt(path, &new_secret, &list_recipients(path)?, Some(message))?;
    Ok(vec![])
}

pub fn find(pattern: &str, ignore_case: &bool) -> Result<Vec<String>> {
    let needle = if *ignore_case { pattern.to_lowercase() } else { pattern.to_string() };
    let mut haystack = list(&None)?;
    haystack.retain(|i| {
        let mut item = i.clone();
        if *ignore_case {
            item = item.to_lowercase();
        }
        item.contains(&needle)
    });
    Ok(haystack)
}

pub fn generate(
    path: &PathBuf,
    length: &Option<u32>,
    symbols: &bool,
    clip: &bool,
    show: &bool,
    user: &Option<String>,
    location: &Option<String>,
) -> Result<Vec<String>> {
    let length = length.unwrap_or(config::get().secret_length()) as usize;
    let generator = PasswordGenerator::new()
        .strict(true)
        .uppercase_letters(true)
        .symbols(*symbols)
        .length(length);

    let secret = generator.generate_one().unwrap();
    add(path, &Some(secret.clone()), user, location)?;
    if *clip {
        copy_to_clipboard(LinuxClipboardKind::Clipboard, secret.clone())?;
    }
    Ok(if *show { vec![secret] } else { vec![] })
}

pub fn list(path: &Option<PathBuf>) -> Result<Vec<String>> {
    let mut result = vec![];
    for path in repository::list(path)? {
        if !path.ends_with(RECIPIENTS_FILENAME) {
            result.push(path);
        }
    }
    Ok(result)
}

pub fn remove(path: &PathBuf) -> Result<Vec<String>> {
    repository::remove(path)
}

pub fn reencrypt(
    path: &PathBuf,
    recipients: Vec<SshKeypair>,
    cached_keys: &mut Vec<SshKeypair>,
) -> Result<()> {
    fill_in_recipients(path, cached_keys)?;
    let identities = get_ssh_identities(cached_keys, 0)?;
    let rcpt_file = path.join(RECIPIENTS_FILENAME);
    let suffix = format!("/{}", RECIPIENTS_FILENAME);
    let mut skipped_collections: Vec<String> = vec![];
    for item in repository::list(&Some(repository::collection_path(path)?))? {
        if item.ends_with(RECIPIENTS_FILENAME) {
            if item != RECIPIENTS_FILENAME && item != rcpt_file.to_str().unwrap() {
                let coll_path = String::from(item.strip_suffix(suffix.as_str()).unwrap());
                println!("Skipping collection {}", coll_path);
                skipped_collections.push(coll_path);
            }
            continue;
        }
        if skipped_collections.iter().any(|c| item.starts_with(c)) {
            continue;
        }
        let path_buf = PathBuf::from(&item);
        let identities = decrypt(&path_buf, &identities)?;
        encrypt(&path_buf, &identities, &recipients, None)?;
    }
    repository::update_file(path, format!("Re-encrypt collection {:?}", path))?;
    Ok(())
}

pub fn show(path: &Path, clip: &bool, cached_keys: &mut Vec<SshKeypair>) -> Result<Vec<String>> {
    let secret = load(path, cached_keys)?;
    if *clip {
        copy_to_clipboard(LinuxClipboardKind::Clipboard, secret.clone())?;
    }
    Ok(vec![secret])
}
