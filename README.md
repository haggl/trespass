# trespass
**t**eam-**r**eady **e**nhanced **s**ubstitute for **pass**

A pass-like secret manager for teams driven by age encryption and SSH keys.
Because gopass is painful, passage lacks support for multiple users and the
author wanted a real-world-problem to solve for learning to code in Rust.

**Warning**:
This project is very much work-in-progress! Expect crashes, weird behavior and
upcoming breaking changes.

Help and/or input from experienced Rust programmers is more than welcome, as
this is my first humble attempt at writing Rust.



## Installation

### Binary

Just download the latest binary to a directory in your shell's search path and
make it executable:
```sh
latest=$(wget -O- https://gitlab.com/api/v4/projects/58927383/repository/tags | jq -r '.[0].name')
sudo wget -O /usr/local/bin/trespass https://gitlab.com/api/v4/projects/58927383/packages/generic/trespass/$latest/trespass-linux-amd64-$latest
sudo chmod +x /usr/local/bin/trespass
```


### From source

To install trespass from source run
```sh
cargo install --path .
```
and add `$HOME/.cargo/bin` to your `$PATH`.


### Shell completions

trespass comes with a built-in generator for shell completion rules. Here is an
example how you could set it up for bash:
```sh
trespass completion bash > $HOME/.local/share/bash-completion/completions/trespass
```
Execute `trespass completion --help` for a list of available generators.


### Daemon auto-launch

If you want systemd to launch `trespass daemon` automatically, set up a user
service and a user socket:
```sh
mkdir -p $HOME/.config/systemd/user
cat <<__EOF__ >$HOME/.config/systemd/user/trespass.service
[Unit]
Description=trespass daemon

[Service]
Environment=SSH_AUTH_SOCK=%t/ssh-agent.socket
ExecStart=%h/.cargo/bin/trespass daemon
Restart=on-failure
__EOF__

cat <<__EOF__ >$HOME/.config/systemd/user/trespass.socket
[Unit]
Description=Socket for trespass daemon

[Socket]
ListenStream=%t/trespass.sock

[Install]
WantedBy=sockets.target
__EOF__
systemctl --user daemon-reload
systemctl --user enable --now trespass.socket
```
Note the `SSH_AUTH_SOCK` environment variable. `ssh-agent` must be run
accordingly, as is done by the `ssh-agent.service`, so if not done already:
```sh
systemctl --user enable ssh-agent.service
echo 'export SSH_AUTH_SOCK=$XDG_RUNTIME_DIR/ssh-agent.socket' >> ~/.zshrc
```

You must also configure the `pinentry` flavor, because the default
`pinentry` script chooses a CLI flavor which obviously won't work:
```sh
mkdir -p $HOME/.config/trespass
cat <<__EOF__ >$HOME/.config/trespass/config.toml
pinentry_program = "pinentry-gnome3"
__EOF__
```


### Configuration

`trespass` reads configuration values from
`$HOME/.config/trespass/config.toml`. The following options are available:

| name                   | type   | description                     | default                             |
| ---------------------- | ------ | ------------------------------- | ----------------------------------- |
| `clip_timeout`         | u64    | Clipboard timeout in seconds    | 30                                  |
| `hist_path`            | string | Interactive history path        | `$XDG_DATA_DIR/trespass/history`    |
| `notification_timeout` | u32    | Notification timeout in seconds | 4                                   |
| `pinentry_program`     | string | Pinentry program                | `pinentry`                          |
| `repo_path`            | string | Secret repository path          | `$XDG_DATA_DIR/trespass/repository` |
| `socket_path`          | string | Unix domain socket path         | `$XDG_RUNTIME_DIR/trespass.sock`    |
| `ssh_path`             | string | SSH pubkey directory path       | `$HOME/.ssh`                        |



## Getting started

### Initialization

The first thing you need is a secret storage repository. There are two ways to get one:

#### Create an empty repository
```sh
trespass repository init $(~/.ssh/id_ed25519.pub)
```

#### Clone an existing repository
```sh
trespass repository clone git@gitlab.com:its-a-me/trespass-store.git
```


### Substores

You can add repositories with shared passwords as substores:
```sh
trespass repository add a-team git@gitlab.com:a-team/trespass-store.git
```
Secrets in this substore will be prefixed with `a-team/`. All secret- and
recipient operations in substores work exactly the same as in the root store.


### Migration from other password managers

Here are some inspirational shell snippets in case you want to import secrets
from another password manager:

#### gopass
Import all secrets, excluding a particular substore:
```sh
for item in $(gopass list --flat | grep -v '^your_substore'); do
  echo $item
  trespass --standalone secret add $item -- "$(gopass show $item)"
done
```

Import all secrets from a substore:
```sh
for secret in $(gopass list --flat --strip-prefix coop); do
    echo $secret
    trespass --standalone secret add $secret -- "$(gopass show coop/$secret)"
done
```



## Metadata

You can store metadata for secrets. Everything after the first line is parsed
as metadata in TOML format. The following secret metadata fields are supported:
* `comment`
* `location`
* `username`

A secret with metadata looks like this:
```toml
Xup3r$3cre7
comment: '''This is a very bad password'''
location: 'https://bad.example.pw'
username: 'its-a-me'
```

See `trespass secret clip --help` for more information on how to clip metadata.


### Converting metadata

From YAML:
```sh
for secret in $(trespass secret list); do
    trespass --standalone secret add $secret "$(trespass secret show $secret \
        | sed "s/url:/location:/; s/\(.*\): \(.*\)/\1 = '''\2'''/")"
done
```
